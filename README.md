# Installation

Add the following at the bottom of your composer.json to access the repo

```
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/neilllandman/larlog.git"
    }
]
```
then run 
`composer require neilllandman/larlog`

To use:
`php artisan landman:larlog`

or use the binary as `vendor/bin/larlog`

Check --help for options
<?php

namespace Landman\LarLog\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

/**
 * Class LarLog
 * @package App\Console\Commands
 */
class LarLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'landman:larlog
{--b|brief  :           Shorten output}
{--s|select :           Show a select menu for log files}
{--file=    :           Specify a log file relative to project_path/storage/logs}
{--c|clear  :           Clear the file first (Use at own risk)}
{--d|debug  :           Print out vars and exit}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sOptions = ['brief', 'select', 'clear', 'debug', 'file'];
        $sOptionString = '';
        foreach ($sOptions as $option) {
            if ($this->option($option)) {
                $sOptionString .= "--$option ";
                if ($option === 'file')
                    $sOptionString .= $this->option('file') . " ";
            }
        }
        $binary = base_path('vendor/bin') . "/./larlog";
        $command = trim("$binary $sOptionString");
        $process = new Process($command);
        $process->setTty(true);
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
                return;
            } else {
                echo $buffer;
            }
        });
    }
}

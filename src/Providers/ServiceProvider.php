<?php

namespace Landman\LarLog\Providers;


use Illuminate\Support\ServiceProvider as SupportServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Landman\LarLog\Console\Commands\LarLog;


/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class ServiceProvider extends SupportServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        // Register commands.
        if ($this->app->runningInConsole()) {
            $this->commands([
                LarLog::class,
            ]);
        }


    }

    /**
     *
     */
    public function register()
    {
    }
}
